with Ada.Strings.Unbounded;
with SDL; use SDL; -- simple SDL types
with SDL.Video.Windows;
with SDL.Video.Windows.Makers;
with SDL.Video.Rectangles;
with SDL.Video.Renderers;
with SDL.Video.Renderers.Makers;
with SDL.Video.Palettes;
with SDL.Events.Events;

package body GNAT_Game.Draw is

   package Windows renames SDL.Video.Windows;
   subtype SDL_Window is Windows.Window;

   package Rectangles renames SDL.Video.Rectangles;
   subtype SDL_Rectangle is Rectangles.Rectangle;
   subtype SDL_Point is SDL.Coordinates;
   subtype SDL_Line is Rectangles.Line_Segment;

   package Renderers renames SDL.Video.Renderers;
   subtype SDL_Renderer is Renderers.Renderer;

   subtype SDL_Palette_Color is SDL.Video.Palettes.Colour;
   subtype SDL_Palette_Color_Component is SDL.Video.Palettes.Colour_Component;

   type Window_And_Canvas_ID (Set : Boolean := False) is record
      case Set is
         when True =>
            Win    : Window_ID;
            Canvas : Canvas_ID; -- GNAT Game's canvas are handled as Canvass
         when False =>
            null;
      end case;
   end record;

   type Window_And_Canvas is limited record
      Win  : SDL_Window;
      Rend : SDL_Renderer;
      ID   : Window_And_Canvas_ID;
   end record;

   --  SDL API around windows and Canvass is complicated
   --  and we only ever need one of each anyway
   Render : Window_And_Canvas;

   -------------
   -- Helpers --
   -------------

   function To_Color (Color : RGBA_T) return SDL_Palette_Color is
     (SDL_Palette_Color_Component (Color.R),
      SDL_Palette_Color_Component (Color.G),
      SDL_Palette_Color_Component (Color.B),
      SDL_Palette_Color_Component (Color.A));

   function Current_Window_Is (ID : Window_ID) return Boolean is
   --  Sanity check for the fact that we have created the window and the
   --  app is using it

     (Render.ID.Win = ID);

   function Current_Canvas_Is (ID : Canvas_ID) return Boolean is
   --  Sanity check for the fact that we have created the window and the
   --  app is using its canvas

     (Render.ID.Canvas = ID);

   function "+" (F : SDL.Init_Flags) return String;

   function To_Size (Width, Height : Positive) return Sizes is
     ((Dimension (Width), Dimension (Height)));

   function To_Rectangle
     (Position : Screen_Point; Width, Height : Positive)
      return SDL_Rectangle is
     ((Coordinate (Position.X), Coordinate (Position.Y), Dimension (Width),
       Dimension (Height)));

   function To_Point (Position : Screen_Point) return SDL_Point is
     (SDL.Coordinate (Position.X), SDL.Coordinate (Position.Y));

   function To_Line (P1, P2 : Screen_Point) return SDL_Line is
     (To_Point (P1), To_Point (P2));

   function "+" (A, B : Screen_Point) return Screen_Point is
     (A.X + B.X, A.Y + B.Y);

   ------------------
   -- Constructors --
   ------------------

   function Create_Window
     (Width : Integer; Height : Integer; Name : String) return Window_ID
   is
      Size : constant Sizes := To_Size (Width, Height);
   begin
      pragma Assert
        (not Render.ID.Set,
         "The current implementation is limited to a single window");

      Render.ID :=
        (Set => True, Win => Window_ID'First, Canvas => Canvas_ID'First);

      Windows.Makers.Create
        (Win  => Render.Win, Title => Name, Position => SDL.Zero_Coordinate,
         Size => Size);

      Renderers.Makers.Create (Render.Rend, Render.Win);

      return Render.ID.Win;
   end Create_Window;

   function Get_Canvas (Window : Window_ID) return Canvas_ID is
   begin
      pragma Assert (Current_Window_Is (Window));

      return Render.ID.Canvas;
   end Get_Canvas;

   ------------------
   -- App Handling --
   ------------------

   package Events renames SDL.Events.Events;
   subtype Event is Events.Events; -- What is not named events at this point...

   App_Killed : Boolean := False;

   procedure Handle_Events is
      Ev : Event;
   begin
      while Events.Poll (Ev) loop
         case Ev.Common.Event_Type is
            when SDL.Events.Quit =>
               App_Killed := True;
            when others =>
               null;
         end case;
      end loop;
   end Handle_Events;

   procedure Swap_Buffers (Window : Window_ID; Erase : Boolean := True) is
   begin
      pragma Assert (Current_Window_Is (Window));

      Swap_Copy_Buffers (Window);

      if Erase then
         Fill (Get_Canvas (Window), Black);
      end if;

      Handle_Events;
   end Swap_Buffers;
   --  Optionaly clear the screen, update the canvas, poll events

   procedure Swap_Copy_Buffers (Window : Window_ID) is
   begin
      pragma Assert (Current_Window_Is (Window));

      Render.Rend.Present;
   end Swap_Copy_Buffers;
   --  Update the canvas
   --  copy the hidden buffer to the visible buffer

   function Is_Killed return Boolean is (App_Killed);

   ------------------------
   -- Drawing Primitives --
   ------------------------

   procedure Fill (Canvas : Canvas_ID; Color : RGBA_T) is
      Size : constant SDL.Sizes := Render.Win.Get_Size;
   begin
      Draw_Fill_Rect
        (Canvas, (0, 0), Positive (Size.Width), Positive (Size.Height), Color);
   end Fill;

   procedure Draw_Circle_Opt
     (Canvas : Canvas_ID; Position : Screen_Point; Radius : Positive;
      Color  : RGBA_T; Filled : Boolean)
   is
      --  https://stackoverflow.com/a/48291620
      Diameter : constant Positive := Radius * 2;

      --  X and Y are counters, X decreases, while Y increases
      --  furthermore, they are delta from the center in the
      --  positive quadrant, so they can't go over the radius
      subtype Radius_Counter is Integer range 0 .. Radius;
      X : Radius_Counter := Radius_Counter'Last;
      Y : Radius_Counter := Radius_Counter'First;

      Tx, Ty : Positive := 1;

      Error : Integer := Tx - Diameter;

      procedure Draw_Segment (P1, P2 : Screen_Point) is
      begin
         if Filled then
            Render.Rend.Draw (To_Line (P1, P2));
         else
            Render.Rend.Draw (To_Point (P1));
            Render.Rend.Draw (To_Point (P2));
         end if;
      end Draw_Segment;
   begin
      Prepare_Pixel_Draw (Canvas, Color);

      if Radius = 1 then
         Draw_Pixel (Canvas, Position);
         return;
      end if;

      while X >= Y loop
         Draw_Segment (Position + (+X, -Y), Position + (+X, +Y));
         Draw_Segment (Position + (-X, -Y), Position + (-X, +Y));
         Draw_Segment (Position + (+Y, -X), Position + (+Y, +X));
         Draw_Segment (Position + (-Y, -X), Position + (-Y, +X));

         if Error <= 0 then
            Y     := Y + 1;
            Error := Error + Ty;
            Ty    := Ty + 2;
         end if;

         if Error > 0 then
            X     := X - 1;
            Tx    := Tx + 2;
            Error := Error + Tx - Diameter;
         end if;
      end loop;
   end Draw_Circle_Opt;

   procedure Draw_Circle
     (Canvas : Canvas_ID; Position : Screen_Point; Radius : Integer;
      Color  : RGBA_T)
   is
   begin
      Draw_Circle_Opt (Canvas, Position, Radius, Color, Filled => False);
   end Draw_Circle;

   procedure Draw_Line
     (Canvas : Canvas_ID; P1 : Screen_Point; P2 : Screen_Point; Color : RGBA_T)
   is
   begin
      Prepare_Pixel_Draw (Canvas, Color);
      Render.Rend.Draw (To_Line (P1, P2));
   end Draw_Line;

   procedure Draw_Text
     (Canvas : Canvas_ID; Position : Screen_Point; Text : String;
      Color  : RGBA_T; Bg_Color : RGBA_T := Black;
      Wrap   : Boolean := True) is null; -- TODO

   procedure Draw_Rect
     (Canvas : Canvas_ID; Position : Screen_Point; Width, Height : Integer;
      Color  : RGBA_T)
   is
      C1 : constant Screen_Point := Position;
      C2 : constant Screen_Point := Position + (Width, 0);
      C3 : constant Screen_Point := Position + (0, Height);
      C4 : constant Screen_Point := Position + (Width, Height);
   begin
      Prepare_Pixel_Draw (Canvas, Color);
      Render.Rend.Draw (To_Line (C1, C2));
      Render.Rend.Draw (To_Line (C1, C3));
      Render.Rend.Draw (To_Line (C2, C4));
      Render.Rend.Draw (To_Line (C3, C4));
   end Draw_Rect;

   procedure Draw_Fill_Rect
     (Canvas : Canvas_ID; Position : Screen_Point; Width, Height : Integer;
      Color  : RGBA_T)
   is
   begin
      Prepare_Pixel_Draw (Canvas, Color);
      Render.Rend.Fill (To_Rectangle (Position, Width, Height));
   end Draw_Fill_Rect;

   procedure Set_Pixel
     (Canvas : Canvas_ID; Position : Screen_Point; Color : RGBA_T)
   is
   begin
      Prepare_Pixel_Draw (Canvas, Color);
      Render.Rend.Draw (To_Point (Position));
   end Set_Pixel;

   ------------
   -- Legacy --
   ------------

   --  Those are disabled and non-supported, they raise exceptions
   --  or redirect to other variants (sphere -> circle)

   procedure Draw_Circle
     (Canvas : Canvas_ID; Position : Point_3d; Radius : Float; Color : RGBA_T)
   is
   begin
      Draw_Circle
        (Canvas, To_Screen_Point (Canvas, Position), Integer (Radius), Color);
   end Draw_Circle;

   procedure Draw_Sphere
     (Canvas : Canvas_ID; Position : Point_3d; Radius : Float; Color : RGBA_T)
   is
   begin
      Draw_Sphere
        (Canvas, (Positive (Position.X), Positive (Position.Y)),
         Positive (Radius), Color); -- 2D engine
   end Draw_Sphere;

   procedure Draw_Sphere
     (Canvas : Canvas_ID; Position : Screen_Point; Radius : Integer;
      Color  : RGBA_T)
   is
   begin
      Draw_Circle_Opt (canvas, Position, Radius, Color, Filled => True);
   end Draw_Sphere;

   procedure Draw_Line
     (Canvas : Canvas_ID; P1 : Point_3d; P2 : Point_3d; Color : RGBA_T)
   is
   begin
      Draw_Line
        (Canvas, To_Screen_Point (Canvas, P1), To_Screen_Point (Canvas, P2),
         Color);
   end Draw_Line;

   procedure Draw_Rect
     (Canvas : Canvas_ID; Position : Point_3d; Width, Height : Float;
      Color  : RGBA_T)
   is
   begin
      Draw_Rect
        (Canvas, To_Screen_Point (Canvas, Position), Integer (Width),
         Integer (Height), Color);
   end Draw_Rect;

   procedure Draw_Fill_Rect
     (Canvas : Canvas_ID; Position : Point_3d; Width, Height : Float;
      Color  : RGBA_T)
   is
   begin
      Draw_Fill_Rect
        (Canvas, To_Screen_Point (Canvas, Position), Integer (Width),
         Integer (Height), Color);
   end Draw_Fill_Rect;

   procedure Enable_3d_Light (Canvas : Canvas_ID) is null; -- 2D engine

   procedure Disable_3d_Light (Canvas : Canvas_ID) is null; -- 2D engine

   procedure Set_3d_Light
     (Canvas        : Canvas_ID; Position : Point_3d; Diffuse_Color : RGBA_T;
      Ambient_Color : RGBA_T) is null; -- 2D engine

   procedure Draw_Text
     (Canvas   : Canvas_ID; Position : Point_3d; Text : String; Color : RGBA_T;
      Bg_Color : RGBA_T  := Black;
      Wrap     : Boolean := True) is null; -- Not supported

   function Get_Text_Size (Text : String) return Screen_Point is
     (raise Unsupported_Feature);

   function Get_Zoom_Factor (Canvas : Canvas_ID) return Float is
     (raise Unsupported_Feature);

   procedure Set_Zoom_Factor (Canvas : Canvas_ID; ZF : Float) is
   begin
      raise Unsupported_Feature;
   end Set_Zoom_Factor;

   procedure Set_Center (Canvas : Canvas_ID; Position : Point_3d) is
   begin
      raise Unsupported_Feature;
   end Set_Center;

   procedure Set_Center (Canvas : Canvas_ID; Position : Screen_Point) is
   begin
      raise Unsupported_Feature;
   end Set_Center;

   function Get_Center (Canvas : Canvas_ID) return Screen_Point is
     (raise Unsupported_Feature);

   function Get_Canvas_Size (Canvas : Canvas_ID) return Screen_Point is
     (raise Unsupported_Feature);

   function Get_Cursor_Status return Cursor_T is (raise Unsupported_Feature);

   function To_Point3d
     (Canvas : Canvas_ID; P : Screen_Point) return Point_3d is
     (raise Unsupported_Feature);

   function To_Screen_Point
     (Canvas : Canvas_ID; P : Point_3d) return Screen_Point is
     (raise Unsupported_Feature);

   --------------
   -- Internal --
   --------------

   pragma Warnings (Off, "formal parameter ""Canvas"" is not referenced");

   procedure Prepare_Pixel_Draw (Canvas : Canvas_ID; Color : RGBA_T) is
   begin
      Render.Rend.Set_Draw_Colour (To_Color (Color));
   end Prepare_Pixel_Draw;

   procedure Draw_Pixel (Canvas : Canvas_ID; Position : Screen_Point) is
   begin
      Render.Rend.Draw (To_Point (Position));
   end Draw_Pixel;

   pragma Warnings (On, "formal parameter ""Canvas"" is not referenced");

   function "+" (F : SDL.Init_Flags) return String is
      use all type Ada.Strings.Unbounded.Unbounded_String;

      S : Ada.Strings.Unbounded.Unbounded_String;

      procedure If_Set_Then_Add
        (S    : in out Ada.Strings.Unbounded.Unbounded_String;
         A, B :        SDL.Init_Flags; Name : String)
      is
      begin
         if (A and B) /= 0 then
            if Length (S) = 0 then
               S := To_Unbounded_String (Name);
            else
               S := " " & To_Unbounded_String (Name);
            end if;
         end if;
      end If_Set_Then_Add;

   begin

      If_Set_Then_Add (S, F, Enable_Timer, "timer");
      If_Set_Then_Add (S, F, Enable_Audio, "audio");
      If_Set_Then_Add (S, F, Enable_Screen, "screen");
      If_Set_Then_Add (S, F, Enable_Joystick, "joystick");
      If_Set_Then_Add (S, F, Enable_Haptic, "haptic");
      If_Set_Then_Add (S, F, Enable_Game_Controller, "game-controller");
      If_Set_Then_Add (S, F, Enable_Events, "events");
      If_Set_Then_Add (S, F, Enable_No_Parachute, "no-parachute");

      return To_String (S);
   end "+";

   procedure Initialize (Flags : SDL.Init_Flags := Enable_Everything) is
      Double_Init : constant SDL.Init_Flags := SDL.Was_Initialised and Flags;
   begin
      pragma Assert
        (Double_Init = 0,
         "tried to initialize the SDL more than once (flag " & (+Double_Init) &
         ")");

      pragma Assert (SDL.Initialise (Flags));
   end Initialize;

begin
   Initialize;
end GNAT_Game.Draw;
