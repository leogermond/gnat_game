with GNAT_Game;      use GNAT_Game;
with GNAT_Game.Draw; use GNAT_Game.Draw;

procedure GNAT_Game.Draw.Draw_Pixels is
   W : Window_ID := Create_Window (800, 600, "Hello");
   C : Canvas_ID := Get_Canvas (W);
begin

   while not Is_Killed loop
      Prepare_Pixel_Draw (C, Green);
      Draw_Pixel (C, (400, 300));
      Swap_Buffers (W);
      delay 0.1;
   end loop;

end GNAT_Game.Draw.Draw_Pixels;
