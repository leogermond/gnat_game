with GNAT_Game;      use GNAT_Game;
with GNAT_Game.Draw; use GNAT_Game.Draw;

procedure Init is
   W : Window_ID := Create_Window (800, 600, "Hello");
begin
   while not Is_Killed loop
      Swap_Buffers (W);
      delay 0.1;
   end loop;
end Init;
