with GNAT_Game;      use GNAT_Game;
with GNAT_Game.Draw; use GNAT_Game.Draw;

procedure Draw_Circle is
   W : Window_ID := Create_Window (800, 600, "Hello");
   C : Canvas_ID := Get_Canvas (W);
begin

   while not Is_Killed loop
      Draw_Circle (C, (400, 300), 300, Red);
      Swap_Buffers (W);
      delay 0.1;
   end loop;
end Draw_Circle;
